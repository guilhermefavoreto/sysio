﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Saida : Operacoes
    {
        public DateTime Horario { get; set; }

        public void opera(string tipo, decimal valor, Dinheiro meuSaldoReal, Dinheiro meuSaldoDollar, List<Dinheiro> lista)
        {
            if (tipo == "R$")
            {
                meuSaldoReal.Valor = meuSaldoReal.Valor - valor;
                var operacao = new Dinheiro()
                {
                    Valor = valor,
                    Data = DateTime.Now,
                    Tipo = "R$",
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
            if (tipo == "$")
            {
                meuSaldoDollar.Valor = meuSaldoDollar.Valor - valor;
                var operacao = new Dinheiro()
                {
                    Valor = valor,
                    Data = DateTime.Now,
                    Tipo = "R$",
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
        }
    }
}