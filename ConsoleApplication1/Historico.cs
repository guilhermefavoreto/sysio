﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Historico
    {
        public void geraHistorico(List<Dinheiro> lista)
        {
            var operacoesEntrada = lista.Where(c => c.TipoOperacao == "Entrada");
            var operacoesSaida = lista.Where(c => c.TipoOperacao == "Saída");
            var operacoesConversaoRealDollar = lista.Where(c => c.TipoOperacao == "Conversao(Real/Dollar)");
            var operacoesConversaoDollarReal = lista.Where(c => c.TipoOperacao == "Conversao(Dollar/Real)");

            foreach(Dinheiro deposito in operacoesEntrada)
            {
                Console.Write("Estes são seus depósitos: ");
                Console.WriteLine(deposito.Tipo + "" + deposito.Valor + ", " + deposito.Data);
            }

            foreach (Dinheiro saque in operacoesSaida)
            {
                Console.Write("Estes são seus saques: ");
                Console.WriteLine(saque.Tipo + "" + saque.Valor + ", " + saque.Data);
            }

            foreach(Dinheiro conversao in operacoesConversaoRealDollar)
            {
                Console.Write("Estas são suas conversões de Real para Dóllar: ");
                Console.WriteLine(conversao.Tipo + ", " + conversao.Data + ", " + conversao.Cotacao);
            }

            foreach (Dinheiro conversao in operacoesConversaoDollarReal)
            {
                Console.Write("Estas são suas conversões de Dóllar para Real: ");
                Console.WriteLine(conversao.Tipo + ", " + conversao.Data + ", " + conversao.Cotacao);
            }
        }   
    }
}
