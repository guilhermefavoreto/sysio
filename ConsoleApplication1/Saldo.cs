﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Saldo
    {   
        public decimal checaSaldo(string tipo, Dinheiro meuSaldoReal, Dinheiro meuSaldoDollar)
        {
            if (tipo == "$")
                return meuSaldoDollar.Valor;
            else
                return meuSaldoReal.Valor;
        }
    }
}
